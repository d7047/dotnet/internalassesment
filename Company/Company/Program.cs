﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Company
{
    internal class Program
    {
        public static void menu()
        {
            Console.WriteLine("1.Hire Manager");
            Console.WriteLine("2.Hire Worker");
            Console.WriteLine("3.Display Information of all the employees");
            Console.WriteLine("4.Update basic salary");
            Console.WriteLine("5.Exit");
        }
        static void Main(string[] args)
        {
            ArrayList al = new ArrayList();


            while (true)
            {

                Console.WriteLine("Enter Choice");
                menu();

                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    
                    case 1:
                        Mgr mgr = new Mgr();
                        mgr.acceptmgr();
                        al.Add(mgr);
                        break;

                   
                    case 2:
                        Worker worker = new Worker();
                        worker.acceptworker();
                        al.Add(worker);
                        break;

                   
                    case 3:
                        Console.WriteLine("=================================================================================");
                        foreach (Emp e in al)
                        {
                            Console.WriteLine(e);
                        }
                        Console.WriteLine("=================================================================================");
                        break;

                    
                    case 4:
                        int empid;
                        Console.WriteLine("Enter empid ");
                        empid = Convert.ToInt32(Console.ReadLine());
                        bool flag = true;

                        foreach (Emp e in al)
                        {
                            if (e.id == empid)
                            {
                                double incrementsal;
                                Console.WriteLine("Enter by how much to increment salary ");
                                incrementsal = Convert.ToDouble(Console.ReadLine());
                                e.basic += incrementsal;
                                flag = false;
                            }

                        }
                        if (flag)
                        {
                            Console.WriteLine("Invalid empid");
                        }

                        break;

                    case 5:
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Invalid Choice...!!! please choose from the below menu options");
                        menu();
                        break;

                }




            }
        }
    }
}
